import configparser
import os.path as op


class ParseFile:
    def __init__(self):
        config = configparser.ConfigParser()

        self.ROOT = op.abspath(op.dirname(__name__))
        self.ROOT_OF_ROOT = op.abspath(op.join(self.ROOT, op.pardir))
        self.INI_PATH = self.ROOT_OF_ROOT + '/modelArgs.ini'

        # parsed information - search api
        self.readConfig = config.read(self.INI_PATH)
        self.output = self.readConfig['MODELARGS']['DATASET']
        self.api_key = self.readConfig['SEARCHAPI']['API_KEY']
        self.max_results = self.readConfig['SEARCHAPI']['MAX_RESULTS']
        self.group_size = self.readConfig['SEARCHAPI']['GROUP_SIZE']
        self.list_of_items = self.readConfig['SEARCHITEMS']['list_of_items']
        self.translated_container = []

        # parsed information - training
        self.model = self.readConfig['MODELARGS']['MODEL']
        self.label_bin = self.readConfig['MODELARGS']['LABEL_BIN']
        self.plot_img = self.readConfig['MODELARGS']['PLOT_IMG']
        self.example_image = self.readConfig['MODELARGS']['EXAMPLE_IMG']

        # translate list items into strings, insert into new list
        for items in self.list_of_items:
            self.translated_container.append(str(items))
        del self.list_of_items

    def for_search_api(self):
        return self.output, self.api_key, self.max_results, self.group_size, self.translated_container

    def for_training(self):
        return self.output, self.model, self.label_bin, self.plot_img

    def for_classifier(self):
        return self.model, self.label_bin, self.example_image
