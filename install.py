import subprocess, sys

pythonLibraries = ['tensorflow', 'pyaudio', 'opencv-python', 'face_recognition', 'speechrecognition',
                   'tflearn', 'beautifulsoup', 'imutils']

def installPythonLibraries(pkg):
    if sys.version_info == (3, 7):
        subprocess.run(['pip', 'install', '--upgrade', '{}'.format(pkg)], capture_output=subprocess.PIPE)
    else:
        subprocess.run(['pip', 'install', '--upgrade', '{}'.format(pkg)], stdout=subprocess.PIPE)


if __name__ == '__main__':
    if sys.platform.startswith('darwin') and sys.version_info >= (3, 4):
        print('This file will install all dependencies for ModelAPI. Please do not interrupt.')

        for lib in pythonLibraries:
            installPythonLibraries(lib)

        print('Installation complete.')

    else:
        print('MacOS is the only supported platform with Python version 3.4 or higher.'
              '\nYour platform & python version: {0} with python {1}.{2}.{3}'.format(sys.platform,
                                                                                     sys.version_info.major,
                                                                                     sys.version_info.minor,
                                                                                     sys.version_info.micro))
        exit(0)
