# USAGE
# put information in modelArgs.ini file, in the correct places.
# run search_bing_api.py - everything else done automatically

# import the necessary packages
from requests import exceptions
from pyimagesearch import parse_file
import train
import classify
import requests
import cv2
import os


def main(query, output, api_key, max_results, group_size):
    if not os.path.isdir(output):
        raise AssertionError('Dataset must be a directory')
    elif query is None or '':
        raise AssertionError('Query option cannot be blank.')

    # set the endpoint API URL
    URL = "https://api.cognitive.microsoft.com/bing/v7.0/images/search"
    
    # when attemping to download images from the web both the Python
    # programming language and the requests library have a number of
    # exceptions that can be thrown so let's build a list of them now
    # so we can filter on them
    EXCEPTIONS = {IOError, FileNotFoundError, exceptions.RequestException, exceptions.HTTPError, exceptions.ConnectionError,
                  exceptions.Timeout}
    
    # store the search term in a convenience variable then set the
    # headers and search parameters
    term = query
    headers = {"Ocp-Apim-Subscription-Key": api_key}
    params = {"q": term, "offset": 0, "count": group_size}
    
    # make the search
    print("[INFO] searching Bing API for '{}'".format(term))
    search = requests.get(URL, headers=headers, params=params)
    search.raise_for_status()
    
    # grab the results from the search, including the total number of
    # estimated results returned by the Bing API
    results = search.json()
    estNumResults = min(results["totalEstimatedMatches"], max_results)
    print("[INFO] {} total results for '{}'".format(estNumResults,
                                                    term))
    
    # initialize the total number of images downloaded thus far
    total = 0
    
    # loop over the estimated number of results in `group_size` groups
    for offset in range(0, estNumResults, group_size):
        # update the search parameters using the current offset, then
        # make the request to fetch the results
        print("[INFO] making request for group {}-{} of {}...".format(
            offset, offset + group_size, estNumResults))
        params["offset"] = offset
        search = requests.get(URL, headers=headers, params=params)
        search.raise_for_status()
        results = search.json()
        print("[INFO] saving images for group {}-{} of {}...".format(
            offset, offset + group_size, estNumResults))
    
        # loop over the results
        for v in results["value"]:
            # try to download the image
            try:
                # make a request to download the image
                print("[INFO] fetching: {}".format(v["contentUrl"]))
                r = requests.get(v["contentUrl"], timeout=30)
    
                # build the path to the output image
                ext = v["contentUrl"][v["contentUrl"].rfind("."):]
                p = os.path.sep.join([output, "{}{}".format(
                    str(total).zfill(8), ext)])
    
                # write the image to disk
                f = open(p, "wb")
                f.write(r.content)
                f.close()
    
            # catch any errors that would not unable us to download the
            # image
            except Exception as e:
                # check to see if our exception is in our list of
                # exceptions to check for
                if type(e) in EXCEPTIONS:
                    print("[INFO] skipping: {}".format(v["contentUrl"]))
                    continue
    
            # try to load the image from disk
            image = cv2.imread(p)
    
            # if the image is `None` then we could not properly load the
            # image from disk (so it should be ignored)
            if image is None:
                print("[INFO] deleting: {}".format(p))
                os.remove(p)
                continue
    
            # update the counter
            total += 1
    print('Done searching.')


if __name__ == '__main__':
    pf = parse_file.ParseFile()

    print('Beginning search...')
    output, api_key, max_results, group_size, list_of_items = pf.for_search_api()
    for item in list_of_items:
        main(list_of_items[item], output, api_key, max_results, group_size)
        
    print('Beginning training...')
    dataset, model_t, label_bin, plot_img = pf.for_training()
    train.main(dataset, model_t, label_bin, plot_img)

    print('Beginning classification test...')
    model_c, labels, example_img = pf.for_classifier()
    classify.main(model_c, labels, example_img)
