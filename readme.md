# Model API
Author: Jaylen A. Douglas (JnJ)<br />
Homepage: https://gitlab.com/abledigital/modelapi (.git) <br />
Description: <br />
This library uses the Bing Image Search API to parse the web for images to train for a image classification model. I've included my personal Bing Search API key but if you would like to use your own or create your own api with Bing please follow this link: https://docs.microsoft.com/en-us/azure/cognitive-services/bing-image-search/. This library is automated and the only file you need to edit is the modelArgs.ini file with the information that you need.

# Required
Python 3, OpenCV-python, Keras, Numpy, imutils, matplotlib, sklearn

# Installing requirements
I've included a file called 'install.py' that will install all dependencies for you. Please run that script then finish reading.

# ModelArgs.ini
This is a standard configuration file used to store information then later parse for use. In the 'SEARCHITEMS' section of the file you'll see an array, please (in string format) put what images you need to Bing to find (i.e list = ['cat', 'dog']). The dataset section should be a directory you would like the images to be downloaded to. You can change the name of the model, labels or plot if you'd like but they can stay as it if not. 

# Running the program
1. Edit the modelArgs.ini file <b>(searchitems & dataset may NOT be left blank)</b>
2. Run this script in your terminal: <b>python3 search_bing_api.py</b>